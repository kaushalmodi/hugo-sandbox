+++
title = "Globs for subdirs in bundles in resources -> src front-matter 4"
author = ["Kaushal Modi"]
date = 2018-01-22T11:07:00-05:00
tags = ["not-a_bug"]
draft = false
[[resources]]
  src = "**copy-of-*.png"
  title = "First copy of Org mode logo"
[[resources]]
  src = "**copy-2-*.png"
  title = "Second copy of Org mode logo"
+++

This page makes use of the [**super-asterisk** support](http://gohugo.io/news/0.34-relnotes/) introduced in Hugo
v0.34.


## Images {#images}

-   `:src "**copy-of-*.png"` --- "works\*
    {{<figure src="images/copy-of-unicorn-logo.png">}}
-   `:src "**copy-2-*.png"`
    {{<figure src="copy-2-of-unicorn-logo.png">}}


## Summary {#summary}

-   `:src "images/copy-of-*.png"` --- **works** ([here](/bugs/globs-in-resources-src-1))
-   `:src "*/copy-of-*.png"` --- **works** ([here](/bugs/globs-in-resources-src-2))
-   `:src "copy-of-*.png"` --- does **not** work ([here](/bugs/globs-in-resources-src-3))
-   `:src "**copy-of-*.png"` --- **works** (this page)

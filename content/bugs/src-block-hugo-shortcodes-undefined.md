+++
title = "Using undefined shortcodes"
date = 2017-11-02T12:48:00-04:00
draft = false
+++

## Using an undefined shortcode below! {#using-an-undefined-shortcode-below}

**To see the issue, remove the `x` from `{x{` below, and run `hugo
 server`.**

```md
{x{< img src="foo.png" >}}
```


### Issue {#issue}

-   The website will generate fine, but with a silent error on the
    terminal:

    ```text
    ERROR 2017/11/02 10:37:54 Unable to locate template for shortcode "img" in page "posts/src-block-hugo-shortcoded-undefined.md"
    ```
-   The `hugo server` run server will keep on living, and generating the
    pages _almost correctly_, while just balking on shortcode
    expansions!


## With special shortcode comment chars {#with-special-shortcode-comment-chars}

Now, as an invalid shortcode `img` was used above, none of the
following short codes seem to get processed. So {&lbrace;</\* .. \*/>}&rbrace;
appears as it is below.

```md
{{</* figure src="/images/org-mode-unicorn-logo.png" title="Org Mode + Hugo Rock!" */>}}
```


## Without special shortcode comment chars {#without-special-shortcode-comment-chars}

Now, as an invalid shortcode `img` was used above, none of the
following short codes seem to get processed. So even {&lbrace;< .. >}&rbrace;
appears as it is below.

```md
{{< figure src="/images/org-mode-unicorn-logo.png" title="Org Mode + Hugo Rock!" >}}
```


## So, what should be fixed? {#so-what-should-be-fixed}

I think, that if an invalid shortcode is found, Hugo should simply
abort the `hugo server`, so that a user doesn't think that _things are
good_. This issue [left me confused for a while as to why the
shortcodes with comment chars were behaving differently](https://discourse.gohugo.io/t/how-is-the-hugo-doc-site-showing-shortcodes-in-code-blocks/9074).

It's good that `hugo` aborts right away on finding this undefined
shortcode `img`, but `hugo server` keeps on living.

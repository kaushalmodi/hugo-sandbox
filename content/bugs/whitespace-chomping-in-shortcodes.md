+++
title = "Whitespace chomping inside shortcodes"
author = ["Kaushal Modi"]
description = "Use of whitespace chomping braces in shortcodes."
date = 2018-04-23T08:46:00-04:00
tags = ["chomp", "whitespace", "shortcode"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.11 + ox-hugo)"
+++

[Ref](https://discourse.gohugo.io/t/using-inner-still-adds-newlines/11679)


## Shortcode definition (`inner-chomp`) {#shortcode-definition--inner-chomp}

```go-html-template
<div>{{- .Inner -}}</div>
```


## Shortcode use {#shortcode-use}

```md
{{</* inner-chomp */>}}
Hello
{{</* /inner-chomp */>}}
```

{{< inner-chomp >}}
Hello
{{< /inner-chomp >}}


## Expected HTML {#expected-html}

```html
<div>Hello</div>
```


## Observed HTML {#observed-html}

_View the source of this page and search for "Hello"._

```html
<div>
Hello
</div>
```


## Workaround {#workaround}

The workaround is to put the opening/closing shortcode identifiers on
the same line.

```md
{{</* inner-chomp */>}}Workaround{{</* /inner-chomp */>}}
```

_View the source of this page and search for "Workaround"._

{{< inner-chomp >}}Workaround{{< /inner-chomp >}}

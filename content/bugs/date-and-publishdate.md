+++
title = "date and publishdate"
author = ["Kaushal Modi"]
publishDate = 2018-01-23T00:00:00-05:00
tags = ["fixed"]
draft = false
+++

Hugo Issue #[2145](https://github.com/gohugoio/hugo/issues/2145), Hugo Issue #[4323](https://github.com/gohugoio/hugo/issues/4323)

This post has only the `publishdate` set in the front-matter, not the
`date`.

+++
title = "Globs for subdirs in bundles in resources -> src front-matter 3"
author = ["Kaushal Modi"]
date = 2018-01-19T12:30:00-05:00
tags = ["not-a_bug"]
draft = false
[[resources]]
  src = "copy-of-*.png"
  title = "First copy of Org mode logo"
[[resources]]
  src = "copy-2-*.png"
  title = "Second copy of Org mode logo"
+++

Test case for Hugo Issue #[4295](https://github.com/gohugoio/hugo/issues/4295):


## Images {#images}

-   `:src "copy-of-*.png"` --- does **not** work
    {{<figure src="images/copy-of-unicorn-logo.png">}}
-   `:src "copy-2-*.png"`
    {{<figure src="copy-2-of-unicorn-logo.png">}}


## Summary {#summary}

-   `:src "images/copy-of-*.png"` --- **works** ([here](/bugs/globs-in-resources-src-1))
-   `:src "*/copy-of-*.png"` --- **works** ([here](/bugs/globs-in-resources-src-2))
-   `:src "copy-of-*.png"` --- does **not** work (this page)
-   `:src "**copy-of-*.png"` --- **works** ([here](/bugs/globs-in-resources-src-4))

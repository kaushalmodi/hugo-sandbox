+++
title = "HTML entities in code blocks"
author = ["Kaushal Modi"]
date = 2017-12-21T11:35:00-05:00
draft = false
+++

Hugo Issue #[4179](https://github.com/gohugoio/hugo/issues/4179)

```html
<Foo attr=" &lt; "></Foo>
```

+++
title = "Articles Landing Index page"
draft = false
+++

Landing page.

-   [Sub-section 1](./sub1)
-   [Sub-section 2](./sub2)

_(Above manually linking is a quick hack. Ideally you would get the
links of nested branch bundles in the Hugo layout.)_

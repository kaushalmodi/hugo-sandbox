+++
title = "Articles sub-section 1 Index page"
draft = false
+++

Sub-section 1.

I agree that `ox-hugo-issue-210b` has to be repeated here. But really,
is it practically that inconvenient to repeat just the section names?

[Ref](https://github.com/kaushalmodi/ox-hugo/issues/210#issuecomment-424494895)

[scripter.co -- Leaf and Branch Bundles](https://scripter.co/hugo-leaf-and-branch-bundles/)

+++
title = "Internal shortcode for table"
author = ["Kaushal Modi"]
draft = false
+++

{{% table %}}

| a | b |
|---|---|
| c | d |

{{% /table %}}

{{% table style="width:50%" %}}

| a | b |
|---|---|
| c | d |

{{% /table %}}

<style>

.toto { border-collapse: collapse; }

.toto table, .toto th, .toto td { border: 1px solid black; padding: 15px; text-align: left; }

</style>

{{% table class="toto" %}}

| a | b |
|---|---|
| c | d |

{{% /table %}}

{{% table class="toto" style="width:50%; border: 1px solid green" %}}

| a | b |
|---|---|
| c | d |

{{% /table %}}

<style>.zuzu { width:50%; } .zuzu th, .zuzu td { padding: 30px; text-align: right; }</style>

{{% table "zuzu" %}}

| a | b |
|---|---|
| c | d |

{{% /table %}}

{{% table "lala" "border:3px dashed red;" %}}

| a | b |
|---|---|
| c | d |

{{% /table %}}

+++
title = "Image captions with Markdown"
author = ["Kaushal Modi"]
date = 2018-02-13T10:31:00-05:00
draft = false
+++

{{< figure src="/images/magit-log-buffer-file.png" caption="Log buffer for `gohugoio/hugo/tpl/tplimpl/template_embedded.go` created by **Magit** on doing `M-x magit-log-buffer-file`" >}}


## References {#references}

-   [Thread on Hugo Discourse](https://discourse.gohugo.io/t/markdown-in-figure-captions/10523/2)
-   `hugo` PR #[4405](https://github.com/gohugoio/hugo/pull/4405)
-   `hugo` Issue #[4406](https://github.com/gohugoio/hugo/issues/4406)

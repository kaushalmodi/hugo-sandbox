+++
title = "Page Resource 1 in Leaf Bundle 1 in \"bundle-nesting\" section"
author = ["Kaushal Modi"]
date = 2018-01-30
tags = ["bundle-nesting", "leaf-bundle", "page-resource"]
draft = false
+++

Content in Page Resource 1 of Leaf bundle l1 in section
"bundle-nesting". **This is not published anywhere.**

+++
title = "Bundle Nesting"
author = ["Kaushal Modi"]
date = 2018-01-30
tags = ["bundle-nesting", "section"]
draft = false
+++

This is content in section "bundle-nesting".

This is both:

-   Default section as it is the first directory level in `content/`.
-   Branch bundle based section as the directory has an `_index.md` file
    too (where this content is written).

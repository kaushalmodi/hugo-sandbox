+++
title = "Index page of Branch Bundle 2 in Branch Bundle 1"
author = ["Kaushal Modi"]
date = 2018-01-30
tags = ["bundle-nesting", "bb1", "branch-bundle", "bb2", "_index"]
draft = false
+++

This is content from "branch-bundle-2" `_index` page, which is inside
"branch-bundle-1".

**This is a branch bundle based section.**

+++
title = "Home Kind"
author = ["Kaushal Modi"]
description = "Home Branch Bundle (CANNOT be Leaf)"
tags = ["home"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.7 + ox-hugo)"
[[resources]]
  src = "*featured*"
  [resources.params]
    credit = "meme.el"
    size = 600
    caption = "Generated in emacs"
    url = "https://github.com/larsmagne/meme"
+++

```text
content/
 - _index.md
 - homebranchbundle-featured.png
```

<style>.page-kinds table tr:nth-child(1) td {background: yellow;}</style>

<div class="ox-hugo-table page-kinds">
<div></div>

| `Kind`         | Description                                                        | Example                                                                       |
|----------------|--------------------------------------------------------------------|-------------------------------------------------------------------------------|
| `home`         | The home page                                                      | `/index.html`                                                                 |
| `page`         | A page showing a _regular page_                                    | `my-post` page (`/posts/my-post/index.html`)                                  |
| `section`      | A page listing _regular pages_ from a given [_section_](/sections) | `posts` section (`/posts/index.html`)                                         |
| `taxonomy`     | A page listing _regular pages_ from a given _taxonomy term_        | page for the term `awesome` from `tags` taxonomy (`/tags/awesome/index.html`) |
| `taxonomyTerm` | A page listing terms from a given _taxonomy_                       | page for the `tags` taxonomy (`/tags/index.html`)                             |

</div>

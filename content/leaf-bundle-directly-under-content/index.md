+++
title = "Leaf Bundles not allowed directly under content/"
author = ["Kaushal Modi"]
date = 2018-01-26T14:14:00-05:00
tags = ["fixed"]
draft = false
+++

**Update <span class="timestamp-wrapper"><span class="timestamp">&lt;2018-01-27 Sat&gt;</span></span>: Fixed**

Hugo Issue #[4332](https://github.com/gohugoio/hugo/issues/4332)

This leaf bundle lives directly under the `content/` directory.

It's a leaf bundle, but gets rendered using the `list` layout instead
of `single` layout.

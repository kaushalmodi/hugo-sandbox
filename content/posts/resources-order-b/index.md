+++
title = "Resource Order B"
author = ["Kaushal Modi"]
date = 2018-01-22T14:48:00-05:00
tags = ["resources"]
draft = false
[[resources]]
  src = "documents/guide.pdf"
  title = "Instruction Guide"
  [resources.params]
    ref = 90564568
[[resources]]
  src = "documents/checklist.pdf"
  title = "Document Checklist"
  [resources.params]
    ref = 90564572
[[resources]]
  src = "documents/photo_specs.pdf"
  title = "Photo Specifications"
  [resources.params]
    ref = 90564687
[[resources]]
  src = "documents/payment.docx"
  title = "Proof of Payment"
[[resources]]
  src = "documents/*.pdf"
  [resources.params]
    icon = "pdf"
[[resources]]
  src = "documents/*.docx"
  [resources.params]
    icon = "word"
+++

[**Markdown source**](https://gitlab.com/kaushalmodi/hugo-sandbox/raw/master/content/posts/resources-order-b/index.md)

---

Source of documents: [1](https://regisphilibert.com/bogus/application/), [2](https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/)

[Ref](https://discourse.gohugo.io/t/resources-metadata-rule-ordering/10140)

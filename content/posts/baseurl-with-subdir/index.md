+++
title = "BaseURLs with subdirs and absURL/relURL"
author = ["Kaushal Modi"]
tags = ["absURL", "relURL", "baseURL", "subdir", "canonifyURLs"]
categories = ["miscellaneous"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.9 + ox-hugo)"
+++

[Discussion](https://discourse.gohugo.io/t/baseurl-path-subdir-sometimes-ignored-by-relurl-absurl/11512)

| Test Site Description                                                    | Link                                                      |
|--------------------------------------------------------------------------|-----------------------------------------------------------|
| Test site with `baseURL` with subdir -- `canonifyURLs = false` (default) | <https://hugo-sandbox.netlify.com/subdir-canonify-false/> |
| Test site with `baseURL` with subdir -- `canonifyURLs = true`            | <https://hugo-sandbox.netlify.com/subdir-canonify-true/>  |

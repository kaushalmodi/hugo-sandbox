+++
title = "Casing of Front-matter Keys (Leaf Bundle)"
author = ["Kaushal Modi"]
layout = "fm-casing"
tags = ["front-matter", "casing", "leaf", "bundle", "page-bundle"]
categories = ["miscellaneous", "discourse"]
type = "dummy"
draft = false
lowercase = 5
UpperCamelCase = 6
lowerCamelCase = 7
snake_case = 8
+++

This is a leaf bundle Index page.

[Discussion](https://discourse.gohugo.io/t/arent-all-front-matter-parameters-lower-cased-internally/11050)

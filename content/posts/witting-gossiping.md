+++
title = "Page witting gossiping"
author = ["Kaushal Modi"]
tags = ["relref"]
categories = ["miscellaneous"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.13 + ox-hugo)"
+++

[Reference](https://mastodon.technology/@kaushalmodi/99998893890382371)

-   [Link to self (posts/witting-gossiping)]({{< relref "witting-gossiping" >}})
-   [Link to posts/bleariest-stubble]({{< relref "bleariest-stubble" >}})
-   [Link to posts/foo/rotary-balm]({{< relref "rotary-balm" >}})

+++
title = "Resource Order C"
author = ["Kaushal Modi"]
date = 2018-01-22T14:48:00-05:00
tags = ["resources"]
draft = false
[[resources]]
  src = "documents/guide.pdf"
  title = "Instruction Guide"
  [resources.params]
    ref = 90564568
    icon = "pdf"
[[resources]]
  src = "documents/checklist.pdf"
  title = "Document Checklist"
  [resources.params]
    ref = 90564572
    icon = "pdf"
[[resources]]
  src = "documents/photo_specs.pdf"
  title = "Photo Specifications"
  [resources.params]
    ref = 90564687
    icon = "pdf"
[[resources]]
  src = "documents/payment.docx"
  title = "Proof of Payment"
  [resources.params]
    icon = "word"
+++

**The only way this would work.**

[@bep](https://discourse.gohugo.io/t/resources-metadata-rule-ordering/10140/3?u=kaushalmodi)
: As I have mentioned in other placing: There is no merge logic, so
            the first name, title and params win.

_So the "solution" is to just put all `params` specific to a file in
the same `resources` → `src` block._

[**Markdown source**](https://gitlab.com/kaushalmodi/hugo-sandbox/raw/master/content/posts/resources-order-c/index.md)

---

Source of documents: [1](https://regisphilibert.com/bogus/application/), [2](https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/)

[Ref](https://discourse.gohugo.io/t/resources-metadata-rule-ordering/10140)

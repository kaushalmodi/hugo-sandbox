+++
title = "Notes on HEP course"
author = ["Vidhya Sagar V"]
date = 2018-08-07T00:00:00-04:00
draft = false
+++

## Nuclear Physics {#nuclear-physics}


### Stable nuclei, Nomenclature and units {#stable-nuclei-nomenclature-and-units}

-   Atomic scale is 10<sup>-10</sup>m, nuclear scale is 10<sup>-15</sup>m. For convinience we use the unit **fermi** (fm) = 10<sup>-15</sup>m. Nuclear sizes range from 1 fm to 7 fm.  Particle physics usually happens at an even smaller scale <<10<sup>-15</sup>m.
-   A nuclear species or _nuclide_ is denoted by \\(^A\_ZX\_N\\). Here, X is the chemical symbol. Z is the **atomic number**: the number of protons. A is the **mass number**: the integer nearest to the ratio between the nuclear mass and the fundamental mass unit (1/12 th of mass of \\(^{12}\_6C\\)). N = A-Z represents the number of neutrons.
-   Before the discovery of neutrons, it was believed that nucleas contains A protons along with A-Z electrons to justify the charge. But it is unsatisfactory because: A force stronger than Coulomb is required between protons and electrons; Confining electrons in 10<sup>-14</sup>m requires momentum of range 20 MeV/c (acc. to Uncertainity principle), but &beta; rays usually have energies less than 1 MeV; total intrinsic angular momentum (spin) of nuclei (A-Z) would disgree with spin addition of A protons and A-Z electons; nuclei containing unpaired electrons would be expected to have magnetic dipole moments greated than oberved.
-   **Isotopes**: Nuclides with same Z but different N (and A). Like \\(^{35}C\\) and \\(^{37}C\\).
-   Radioisotopes/radioactive isotopes: unstable isotopes artificially produced in nuclear reactions.
-   **Isotones**: Nuclides with same N but different Z (so X,A) like \\(^2H\\) and \\(^3He\\).
-   **Isobars**: Nuclides with same A like \\(^3He\\) and \\(^3H\\).
-   The properties of nuclides we measure include: mass, radius, relative abundance (for stable), decay modes and half-lives (for radioactive), reaction modes and cross sections, spin, magnetic dipole and electric quadrapole moments, and excited states.
-   Black = stable (radioactive lifetime is huge), Grey = Unstable
    ![](./HEP/stable.jpeg)
-   Typical &beta; and &gamma; decay energies are in the range of 1 MeV, and low-energy nuclear reactions take place with kinetic energies of order 10 MeV which are far smaller than nuclear rest energies. So nonrelativistic formulation is justfied for nucleons, but &beta; -decay electrons must be treated relativistically.
-   Electromagnetic (&gamma;) decays generally occur with lifetimes of order nanoseconds to picoseconds. &alpha; and &beta; decays occure with longer lifetimes, often minutes or hours. Many nuclear reactions (\\(^5He\\) or \\(^8Be\\) breaking apart) take place in the order of 10<sup>-20</sup> s, which is roughly the time that the reacting nuclei are within range of each other's nuclear force.


#### Remember {#remember}

-   Z=92 for U, 26 for Fe.
-   1 u = 931.502 MeV = 1.661 \\(\times 10^{-27}\\) Kg.


#### Further Reading {#further-reading}

-   Geiger and Marsden experiment confirming existence of atomic nucleus.
-   How does smoke detector's working depend on nuclear physics.


### Size and shape of nuclei {#size-and-shape-of-nuclei}

-   Like the radius of an atom, the radius of a nucleas is not precisely defined. The density of nucleons and the nuclear potential have similar spatial dependence - relatively constant over short distances beyond whic thay drop rapidly to zero.
-   (Spherical) nucler shape is characterized by: **mean radius**, where the density is half its central value, and the **skin thickness** over which the density drops from near its maximum to near its minimum.
-   In some experiments like high-energy electron scattering, muonic X rays, optical and X-ray isotope shifts, and energy differences of mirror nuclei where we measure Coulomb interaction of a charged particle with the nucleus we determine the _distribution of nuclear charge_ (primarily distribution of protons but also involving somewhat that of neutrons). In other experiments such as Rutherford scattering, &alpha; decay, and pionic X rays, we measure the strong nuclear interaction we would determine the _distribution of nuclear matter_.
-   Shape and size of an object is determined by examining the radiation scattered from it for which we need wavelength smaller than the details of the object. For nuclei with diameter 10 fm, we require &lambda; <= 10 fm i.e., p >= 100 MeV/c.


## Clarifications {#clarifications}

-   The moodle says 10 best out of 11 assignments. But 8 out of 11 is mentioned in class.


## References {#references}

-   [Discussion on Hugo Discourse](https://discourse.gohugo.io/t/how-to-use-org-mode-with-hugo/6430/13?u=kaushalmodi)
-   [Original source](https://gitlab.com/vidyasagarv/casper-cms-template/raw/11434160719317cb410621cd774715b2779cecdc/site/content/post/HEP.org)

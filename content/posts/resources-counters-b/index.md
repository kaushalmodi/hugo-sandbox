+++
title = "Resource Counters B"
author = ["Kaushal Modi"]
date = 2018-01-26T12:45:00-05:00
tags = ["resources", "fixed"]
draft = false
[[resources]]
  src = "**.pdf"
  title = "PDF File #:counter"
  name = "pdf-file-:counter"
  [resources.params]
    foo = "foo-:counter"
+++

`:counter` is set in `title`, `name` and `params`.


## Observation {#observation}

-   Counter increments in `name` ✅
-   Counter increments in `title` ✅
-   Counter increments in `foo` 🚫 --- Probably `:counter` does
    not work in `params`.


## Ref {#ref}

-   Hugo Issue #[4334](https://github.com/gohugoio/hugo/issues/4334) → [discourse discussion](https://discourse.gohugo.io/t/unexpected-replacement-of-counter-in-resources-title-name/10217/1) → Hugo Issue #[4335](https://github.com/gohugoio/hugo/issues/4335)
-   [Ref](https://github.com/gohugoio/hugoDocs/issues/308#issuecomment-360844191)

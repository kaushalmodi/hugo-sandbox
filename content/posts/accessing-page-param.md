---
title: "Accessing page param"
author: ["Kaushal Modi"]
description: "Testing a Page Param with first letter uppercase."
date: 2018-09-25T16:23:00-04:00
categories: ["miscellaneous"]
draft: false
creator: "Emacs 27.0.50 (Org mode 9.1.14 + ox-hugo)"
Editor: "Foo Bar"
---

[ref](https://discourse.gohugo.io/t/accessing-params-from-partial/14413)

{{< debug "params" >}}
{{< debug param="Editor" >}}
{{< debug param="editor" >}}

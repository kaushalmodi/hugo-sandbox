+++
title = "Resource Order A"
author = ["Kaushal Modi"]
date = 2018-01-22T14:48:00-05:00
tags = ["resources"]
draft = false
[[resources]]
  src = "images/header.*"
  name = "header"
[[resources]]
  src = "documents/photo_specs.pdf"
  title = "Photo Specifications"
  [resources.params]
    icon = "photo"
[[resources]]
  src = "documents/guide.pdf"
  title = "Instruction Guide"
[[resources]]
  src = "documents/checklist.pdf"
  title = "Document Checklist"
[[resources]]
  src = "documents/payment.docx"
  title = "Proof of Payment"
[[resources]]
  src = "**.pdf"
  name = "pdf-file-:counter"
  [resources.params]
    icon = "pdf"
[[resources]]
  src = "**.docx"
  [resources.params]
    icon = "word"
+++

[**Markdown source**](https://gitlab.com/kaushalmodi/hugo-sandbox/raw/master/content/posts/resources-order-a/index.md)

---

Source of documents: [1](https://regisphilibert.com/bogus/application/), [2](https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/)

[Ref](https://discourse.gohugo.io/t/resources-metadata-rule-ordering/10140)

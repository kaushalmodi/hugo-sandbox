+++
title = "Resource Counters Name and Title"
author = ["Kaushal Modi"]
tags = ["resources", "counter"]
draft = false
[[resources]]
  src = "*specs.pdf"
  title = "Specifications #:counter"
[[resources]]
  src = "**.pdf"
  name = "pdf-file-:counter"
+++

Testing separate counters for `Name` and `Title`.

+++
title = "Casing of Front-matter Keys (Headless Leaf Bundle)"
author = ["Kaushal Modi"]
layout = "fm-casing"
tags = ["front-matter", "casing", "leaf", "bundle", "page-bundle", "headless"]
categories = ["miscellaneous", "discourse"]
type = "dummy"
draft = false
headless = true
lowercase = 9
UpperCamelCase = 10
lowerCamelCase = 11
snake_case = 12
+++

This is a Headless leaf bundle Index page.

[Discussion](https://discourse.gohugo.io/t/arent-all-front-matter-parameters-lower-cased-internally/11050)

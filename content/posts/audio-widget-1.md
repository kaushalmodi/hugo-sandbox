+++
title = "Audio widget 1"
audio = "/audio/bachfugue.mp3"
author = ["Kaushal Modi"]
date = 2018-10-08T16:11:00-04:00
layout = "audio"
categories = ["miscellaneous"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.14 + ox-hugo)"
+++

[Audio credit](https://ccrma.stanford.edu/~jos/pasp/Sound%5FExamples.html)

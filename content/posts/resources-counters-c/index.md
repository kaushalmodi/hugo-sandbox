+++
title = "Resource Counters C"
author = ["Kaushal Modi"]
date = 2018-01-26T12:45:00-05:00
tags = ["resources", "bug", "fixed"]
draft = false
[[resources]]
  src = "documents/photo_specs.pdf"
  title = "Photo Specifications"
[[resources]]
  src = "**.pdf"
  name = "pdf-file-:counter"
+++

`:counter` is still set only in `name`, but that Name-assignment rule
happens during the second "fetch" of `"documents/photo_specs.pdf"`.


## Observation {#observation}

-   Counter increments in `name` 🚫


## Ref {#ref}

-   Hugo Issue #[4334](https://github.com/gohugoio/hugo/issues/4334) → [discourse discussion](https://discourse.gohugo.io/t/unexpected-replacement-of-counter-in-resources-title-name/10217/1) → Hugo Issue #[4335](https://github.com/gohugoio/hugo/issues/4335)
-   [Ref](https://github.com/gohugoio/hugoDocs/issues/308#issuecomment-360844191)

+++
title = "Casing of Front-matter Keys (Regular Page)"
author = ["Kaushal Modi"]
layout = "fm-casing"
tags = ["front-matter", "casing", "regular-page"]
categories = ["miscellaneous", "discourse"]
type = "dummy"
draft = false
lowercase = 1
UpperCamelCase = 2
lowerCamelCase = 3
snake_case = 4
+++

This is a regular page.

[Discussion](https://discourse.gohugo.io/t/arent-all-front-matter-parameters-lower-cased-internally/11050)

+++
title = "Resource Order Mutually exclusive"
author = ["Kaushal Modi"]
date = 2018-01-22T17:31:00-05:00
tags = ["resources"]
draft = false
[[resources]]
  src = "documents/guide.pdf"
  [resources.params]
    foo = "guide"
[[resources]]
  src = "documents/[!g][!u][!i][!d][!e]*.pdf"
  [resources.params]
    bar = "not guide 5+char"
[[resources]]
  src = "documents/[a-z].pdf"
  [resources.params]
    zoo1 = "not guide 1char"
[[resources]]
  src = "documents/[a-z][a-z].pdf"
  [resources.params]
    zoo2 = "not guide 2char"
[[resources]]
  src = "documents/[a-z][a-z][a-z].pdf"
  [resources.params]
    zoo3 = "not guide 3char"
[[resources]]
  src = "documents/[a-z][a-z][a-z][a-z].pdf"
  [resources.params]
    zoo4 = "not guide 4char"
[[resources]]
  src = "documents/*.docx"
  [resources.params]
    lulu = "docx"
+++

[**Markdown source**](https://gitlab.com/kaushalmodi/hugo-sandbox/raw/master/content/posts/resources-order-mutex/index.md)

---

Source of documents: [1](https://regisphilibert.com/bogus/application/), [2](https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/)

[Ref](https://discourse.gohugo.io/t/resources-metadata-rule-ordering/10140)

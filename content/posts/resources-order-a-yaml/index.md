---
title : "Resource Order A (YAML)"
author : ["Kaushal Modi"]
date : 2018-01-25T14:49:00-05:00
tags : ["resources"]
draft : false
resources :
- src : "images/header.*"
  name : "header"
- src : "documents/photo_specs.pdf"
  title : "Photo Specifications"
  params :
    icon : "photo"
- src : "documents/guide.pdf"
  title : "Instruction Guide"
- src : "documents/checklist.pdf"
  title : "Document Checklist"
- src : "documents/payment.docx"
  title : "Proof of Payment"
- src : "**.pdf"
  name : "pdf-file-:counter"
  params :
    icon : "pdf"
- src : "**.docx"
  params :
    icon : "word"
---

[**Markdown source**](https://gitlab.com/kaushalmodi/hugo-sandbox/raw/master/content/posts/resources-order-a-yaml/index.md)

---

Source of documents: [1](https://regisphilibert.com/bogus/application/), [2](https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/)

[Ref](https://discourse.gohugo.io/t/resources-metadata-rule-ordering/10140)

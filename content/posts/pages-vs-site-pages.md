+++
title = "Pages vs Site Pages"
author = ["Kaushal Modi"]
categories = ["miscellaneous"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.13 + ox-hugo)"
+++

| Variable             | Current context | Pages included                                                                                                                                                                                            |
|----------------------|-----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `.Site.Pages`        | **any**         | ALL pages of the site: content, sections, taxonomies, etc. -- Superset of everything!                                                                                                                     |
| `.Site.RegularPages` | **any**         | Only regular (content) pages -- Subset of `.Site.Pages`                                                                                                                                                   |
| `.Pages`             | _List_ page     | Regular pages under that _list_ page representing the homepage, section, taxonomy term (`/tags`) or taxonomy (`/tags/foo`) page -- Subset of `.Site.Pages` or `.Site.RegularPages`, depending on context. |
| `.Pages`             | _Single_ page   | empty slice                                                                                                                                                                                               |

In the **home** context (`index.html`), `.Pages` is the same as
`.Site.RegularPages`.

Note
: `.Pages` is an alias to `.Data.Pages`. It is conventional to
    use that alias form.

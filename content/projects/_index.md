+++
title = "Taxonomy Term Kind (Projects)"
author = ["Kaushal Modi"]
description = """
  Taxonomy Term Branch Bundle (CANNOT be Leaf, else it becomes of `page`
  _Kind_)
  """
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.7 + ox-hugo)"
[[resources]]
  src = "*featured*"
  [resources.params]
    credit = "meme.el"
    size = 600
    caption = "Generated in emacs"
    url = "https://github.com/larsmagne/meme"
+++

```text
content/
  projects/
   - _index.md
   - taxonomytermbranchbundle-projects-featured.png
```

<mark>**Do not set `projects` front-matter for the `projects` `taxonomyTerm`
page!** Else, will cause infloop.

See [Discourse thread](https://discourse.gohugo.io/t/bug-creating-index-md-for-taxonomyterm-pages-tags-categories-hangs-hugo/11121).</mark>

<style>.page-kinds table tr:nth-child(5) td {background: yellow;}</style>

<div class="ox-hugo-table page-kinds">
<div></div>

| `Kind`         | Description                                                        | Example                                                                       |
|----------------|--------------------------------------------------------------------|-------------------------------------------------------------------------------|
| `home`         | The home page                                                      | `/index.html`                                                                 |
| `page`         | A page showing a _regular page_                                    | `my-post` page (`/posts/my-post/index.html`)                                  |
| `section`      | A page listing _regular pages_ from a given [_section_](/sections) | `posts` section (`/posts/index.html`)                                         |
| `taxonomy`     | A page listing _regular pages_ from a given _taxonomy term_        | page for the term `awesome` from `tags` taxonomy (`/tags/awesome/index.html`) |
| `taxonomyTerm` | A page listing terms from a given _taxonomy_                       | page for the `tags` taxonomy (`/tags/index.html`)                             |

</div>
